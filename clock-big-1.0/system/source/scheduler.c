#include "../scheduler.h"

#include <stddef.h>
#include <stdint.h>

#include "../../common/list/list.h"
#include "../tick.h"

enum scheduler_status {
	TASK_RUNNABLE,
	TASK_READY,
	TASK_RUNNING,
	TASK_STOPPED
};

static struct task_scheduler {
	ticker_t ticker;
	ListHead task_list;
} task_scheduler = {0};

static void time_tick_handler(void);

void scheduler_init(void)
{
	tick_add_ticker(&(task_scheduler.ticker), time_tick_handler);
	INIT_LIST_HEAD(&(task_scheduler.task_list));
}

void scheduler_add_task(task_t *const task, void (*handler)(void *arg),
		void *arg, uint8_t delay, uint8_t period)
{
	task->callback = handler;
	task->arg = arg;
	task->interval = period;
	task->delay = delay;
	task->counter = 0U;
	task->status = TASK_STOPPED;
	INIT_LIST_HEAD(&(task->task_list_member));
	list_add(&(task->task_list_member), &(task_scheduler.task_list));
}

void scheduler_start_task(task_t *const task)
{
	task->counter = task->delay;
	task->status = TASK_RUNNABLE;
}

void scheduler_stop_task(task_t *const task)
{
	task->counter = 0U;
	task->status = TASK_STOPPED;
}

void scheduler_dispatch(void)
{
	task_t *task = NULL;

	list_for_each_entry(task, &(task_scheduler.task_list), task_list_member)
	{
		if (TASK_READY == task->status) {
			task->status = TASK_RUNNING;

			(task->callback)(task->arg);

			if (TASK_RUNNING == task->status) {

				if (ONE_SHOT_TASK == task->interval) {
					task->status = TASK_STOPPED;
				} else {
					task->counter = task->interval;
					task->status = TASK_RUNNABLE;
				}
			}
		}
	}
}

void time_tick_handler(void)
{
	task_t *task = NULL;
	
	list_for_each_entry(task, &(task_scheduler.task_list), task_list_member)
	{
		if (TASK_RUNNABLE == task->status) {
			if (0U < task->counter) {
				--(task->counter);
			} else {
				task->status = TASK_READY;
			}
		}
	}
}

