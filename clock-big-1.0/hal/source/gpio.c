#include "../../common/avr/gpio.h"
#include "../gpio.h"

static bool initialized = false;

void hal_gpio_init(void)
{
	gpio_pin_init(&((gpio_handle_t){DISP_ANODE_0}));
	gpio_pin_init(&((gpio_handle_t){DISP_ANODE_1}));
	gpio_pin_init(&((gpio_handle_t){DISP_ANODE_2}));
	gpio_pin_init(&((gpio_handle_t){DISP_ANODE_3}));
	gpio_pin_init(&((gpio_handle_t){DISP_ANODE_4}));
	gpio_pin_init(&((gpio_handle_t){DISP_ANODE_5}));
	gpio_pin_init(&((gpio_handle_t){DISP_COLON_ANODE_0}));
	gpio_pin_init(&((gpio_handle_t){DISP_COLON_ANODE_1}));
	
	gpio_pin_init(&((gpio_handle_t){BUTTON_0}));
	gpio_pin_init(&((gpio_handle_t){BUTTON_1}));
	gpio_pin_init(&((gpio_handle_t){BUTTON_2}));
	gpio_pin_init(&((gpio_handle_t){BUTTON_3}));
	gpio_pin_init(&((gpio_handle_t){BUTTON_4}));
	gpio_pin_init(&((gpio_handle_t){UNUSED_B5}));
	gpio_pin_init(&((gpio_handle_t){UNUSED_B6}));
	gpio_pin_init(&((gpio_handle_t){UNUSED_B7}));

	gpio_pin_init(&((gpio_handle_t){TWI_SCL}));
	gpio_pin_init(&((gpio_handle_t){TWI_SDA}));
	gpio_pin_init(&((gpio_handle_t){DISP_SEG_F}));
	gpio_pin_init(&((gpio_handle_t){DISP_SEG_E}));
	gpio_pin_init(&((gpio_handle_t){DISP_SEG_D}));
	gpio_pin_init(&((gpio_handle_t){DISP_SEG_C}));
	gpio_pin_init(&((gpio_handle_t){DISP_SEG_B}));
	gpio_pin_init(&((gpio_handle_t){DISP_SEG_A}));
	
	gpio_pin_init(&((gpio_handle_t){TEMPERATURE_SENSOR}));
	gpio_pin_init(&((gpio_handle_t){RELAY_VOLTAGE}));
	gpio_pin_init(&((gpio_handle_t){INFRARED_RECEIVER}));
	gpio_pin_init(&((gpio_handle_t){CLOCK_EXT_INTERRUPT}));
	gpio_pin_init(&((gpio_handle_t){BUZZER_SIMPLE}));
	gpio_pin_init(&((gpio_handle_t){BUZZER_GENERATOR}));
	gpio_pin_init(&((gpio_handle_t){DISP_COLON}));
	gpio_pin_init(&((gpio_handle_t){DISP_SEG_G}));

	initialized = true;
}

bool hal_gpio_init_is_done(void)
{
	return (initialized);
}

