#pragma once

/** \file ********************************************************************
 *
 * obsługa przycisków sterujących
 *
 *****************************************************************************/

#include <stdint.h>

#include "../common/button/button.h"

typedef enum button_function_id {
	BUTTON_NEXT,
	BUTTON_PLUS,
	BUTTON_SET,
	BUTTON_MINUS,
	BUTTON_PREVIOUS
} button_function_id_t;

/**
 * inicjalizuje obsługę przycisku
 *
 * \param tick				wewnętrzna podstawa czasu będąca
 * 					 krotnością systemowej podstawy czasu
 * \param debounce_tick			 czas debouncingu styków będący
 * 					krotnością wewnętrznej podstawy
 * 					 czasu modułu
 * \param double_click_tick		czas oczekiwania na dwuklik będący
 * 					 krotnością wewnętrznej podstawy
 * 					 czasu modułu
 * \param long_press_start_tick		czas do wykrycia długiego wciśnięcia
 * 					 będący krotnością wewnętrznej podstawy
 * 					 czasu modułu
 * \param long_press_hold_repeat_tick	czas repetycji przy długim wciśnięciu
 * 					 będący krotnością wewnętrznej podstawy
 * 					 czasu modułu
 *
 */
void drv_button_init(uint8_t tick, uint8_t debounce_tick,
		uint8_t double_click_tick, uint8_t long_press_start_tick,
		uint8_t long_press_hold_repeat_tick);

/**
 * rejestruje funkcję obsługi zdarzeń dla przycisku
 *
 * \param id 		identyfikator przycisku
 * \param handler	funkcja zwrotna obsługująca zdarzenia
 *
 */
void drv_button_handler(button_function_id_t id, void (*handler)(button_event_t event));

/**
 * uruchamia obsługę przycisku
 *
 */
void drv_button_start(void);

/**
 * zatrzymuje obsługę przycisku
 *
 */
void drv_button_stop(void);

