#include "../../hal/gpio.h"
#include "../gpio.h"

#include <stdbool.h>

void drv_gpio_init(void)
{
	if (false == hal_gpio_init_is_done()) {
		hal_gpio_init();
	}
}

