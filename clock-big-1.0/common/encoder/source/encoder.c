#include "../encoder.h"

#include <stddef.h>

enum {
	ENCODER_START,
	ENCODER_CW1,
	ENCODER_CW2,
	ENCODER_CW3,
	ENCODER_CCW1,
	ENCODER_CCW2,
	ENCODER_CCW3
};

enum {
	ENCODER_CCW_FLAG = 0x40U,
	ENCODER_CW_FLAG = 0x80U
};

// tablica przejść maszyny stanów dekodera zdarzeń
static uint8_t const encoder_transitions[][4U] = {
/* 00		   01		   10		    11 */
/*start*/
{ENCODER_START, ENCODER_CW1, ENCODER_CCW1, ENCODER_START},
/*CW_STEP1*/
{ENCODER_CW2, ENCODER_CW1, ENCODER_START, ENCODER_START},
/*CW_STEP2*/
{ENCODER_CW2, ENCODER_CW1, ENCODER_CW3, ENCODER_START},
/*CW_STEP3*/
{ENCODER_CW2, ENCODER_START, ENCODER_CW3, (ENCODER_START | ENCODER_CW_FLAG)},
/*CCW_STEP1*/
{ENCODER_CCW2, ENCODER_START, ENCODER_CCW1, ENCODER_START},
/*CCW_STEP2*/
{ENCODER_CCW2, ENCODER_CCW3, ENCODER_CCW1, ENCODER_START},
/*CCW_STEP3*/
{ENCODER_CCW2, ENCODER_CCW3, ENCODER_START, (ENCODER_START | ENCODER_CCW_FLAG)}};

void encoder_init(encoder_t *const encoder)
{
	encoder->callback = NULL;
	encoder->status = ENCODER_START;
}

void encoder_register_callback(encoder_t *const encoder,
		void (*callback)(encoder_event_t event))
{
	encoder->callback = callback;
}

void encoder_reset(encoder_t *const encoder)
{
	encoder->status = ENCODER_START;
}

void encoder_decode_event(encoder_t *const encoder, uint8_t code)
{
	static uint8_t const code_mask = 0x03U;
	static uint8_t const status_mask = 0x3FU;
	static uint8_t const direction_mask = 0xC0U;
	
	encoder->status =
			encoder_transitions[encoder->status & status_mask][code
					& code_mask];
	
	switch (encoder->status & direction_mask) {
	case ENCODER_CW_FLAG:
		if (NULL != encoder->callback) {
			(encoder->callback)(ENCODER_CW_EVENT);
		}
		
		break;
		
	case ENCODER_CCW_FLAG:
		if (NULL != encoder->callback) {
			(encoder->callback)(ENCODER_CCW_EVENT);
		}
		
		break;
		
	default:
		;
		break;
	}
}

