#include "../circ_buffer.h"

void circ_buffer_init(circ_buffer_t *const buf, uint8_t *const data,
		uint8_t size)
{
	buf->head = 0U;
	buf->tail = 0U;
	buf->mask = (size - 1U);
	buf->data = data;
}

uint8_t circ_buffer_get_size(circ_buffer_t const *const buf)
{
	return ((buf->mask + 1U));
}

uint8_t circ_buffer_get_number_of_elements(circ_buffer_t const *const buf)
{
	return ((buf->head - buf->tail));
}

bool circ_buffer_is_empty(circ_buffer_t const *const buf)
{
	bool ret = false;
	
	if (0U == (buf->head - buf->tail)) {
		ret = true;
	}
	
	return (ret);
}

bool circ_buffer_is_full(circ_buffer_t const *const buf)
{
	bool ret = false;
	
	if ((uint8_t)(buf->mask + 1U) == (uint8_t)(buf->head - buf->tail)) {
		ret = true;
	}
	
	return (ret);
}

bool circ_buffer_is_error(circ_buffer_t const *const buf)
{
	bool ret = false;
	
	if ((uint8_t)(buf->mask + 1U) <= (uint8_t)(buf->head - buf->tail)) {
		ret = true;
	}
	
	return (ret);
}

void circ_buffer_reset_data(circ_buffer_t *const buf)
{
	buf->head = 0U;
	buf->tail = 0U;
}

void circ_buffer_push_item_in_head(circ_buffer_t *const buf, uint8_t data)
{
	(buf->data)[(buf->head++) & buf->mask] = data;
}

void circ_buffer_push_item_in_tail(circ_buffer_t *const buf, uint8_t data)
{
	(buf->data)[(--buf->tail) & buf->mask] = data;
}

uint8_t circ_buffer_pop_item_nr_from_tail(circ_buffer_t const *const buf,
		uint8_t nr)
{
	return ((buf->data)[(buf->tail + nr) & buf->mask]);
}

uint8_t circ_buffer_pop_item_from_tail(circ_buffer_t *const buf)
{
	return ((buf->data)[(buf->tail++) & buf->mask]);
}

/* In readn instead of calculating the access index as: writeIndex - (Xn + 1)
 * we're using: writeIndex + (~Xn)
 */
uint8_t circ_buffer_pop_item_nr_from_head(circ_buffer_t const *const buf,
		uint8_t nr)
{
	return ((buf->data)[(buf->head + (~nr)) & buf->mask]);
}

uint8_t circ_buffer_pop_item_from_head(circ_buffer_t *const buf)
{
	return ((buf->data)[(--buf->head) & buf->mask]);
}

