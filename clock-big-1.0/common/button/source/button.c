#include "../button.h"

#include <stddef.h>

void button_init(button_t *const button, uint8_t debounce_tick_nr,
		uint8_t double_click_ticks_nr, uint8_t long_start_ticks_nr,
		uint8_t long_repeat_ticks_nr)
{
	button->debounce_tick_nr = debounce_tick_nr;
	button->double_click_ticks_nr = double_click_ticks_nr;
	button->long_start_ticks_nr = long_start_ticks_nr;
	button->long_repeat_ticks_nr = long_repeat_ticks_nr;
	button->tick_cnt = 0U;
	button->long_repeat_tick_cnt = 0U;
	button->debounce_cnt = debounce_tick_nr;
	button->is_pressed = false;
	button->state = 0U;
	button->callback = NULL;
}

void button_register_handler(button_t *const button,
		void (*handler)(button_event_t event))
{
	button->callback = handler;
}

void button_reset(button_t *const button)
{
	button->tick_cnt = 0U;
	button->long_repeat_tick_cnt = 0U;
	button->debounce_cnt = button->debounce_tick_nr;
	button->is_pressed = false;
	button->state = 0U;
}

void button_detect_event(button_t *const button, bool pressed)
{
	/* obsługa licznika czasu */
	if (0U < button->state) {
		++(button->tick_cnt);
	}
	/* debouncing przycisku */
	if (pressed != button->is_pressed) {
		--(button->debounce_cnt);

		if (0U == button->debounce_cnt) {
			button->is_pressed = pressed;
			button->debounce_cnt = button->debounce_tick_nr;
		}

	} else {
		button->debounce_cnt = button->debounce_tick_nr;
	}

	switch (button->state) {
	case 0U:
		/* czekamy na wciśnięcie przycisku */
		if (true == button->is_pressed) {

			if (NULL != button->callback) {
				(button->callback)(BUTTON_PRESS_DOWN_EVENT);
			}

			button->tick_cnt = 0U;
			button->state = 1U;
		}
		
		break;
		
	case 1U:
		/* czekamy na zwolnienie przycisku */
		if (false == button->is_pressed) {

			if (NULL != button->callback) {
				(button->callback)(BUTTON_PRESS_UP_EVENT);
			}

			button->state = 2U;
		} else {
			/* wykrycie długiego wciśnięcia */
			if ((true == button->is_pressed)
					&& ((button->tick_cnt)
							> (button->long_start_ticks_nr))) {

				if (NULL != button->callback) {
					(button->callback)(
							BUTTON_LONG_PRESS_START_EVENT);
				}
				
				button->state = 4U;
			}
		}
		
		break;
		
	case 2U:
		/* czekamy na przekroczenie czasu oczekiwania lub ponownie
		 wciśnięcie przycisku (dwuklik) */
		if ((button->tick_cnt) > (button->double_click_ticks_nr)) {
			/* to było pojedyńcze wciśnięcie */
			if (NULL != button->callback) {
				(button->callback)(BUTTON_SINGLE_CLICK_EVENT);
			}
			
			button->state = 0U;
		} else {
			/* jeśli ponownie wciśnięty przycisk przed upływem czasu
			 oczekiwania na dwuklik */
			if (true == button->is_pressed) {

				if (NULL != button->callback) {
					(button->callback)(
							BUTTON_PRESS_DOWN_EVENT);
				}

				button->state = 3U;
			}
		}
		
		break;
		
	case 3U:
		/* oczekiwanie na ostateczne zwolnienie przycisku */
		if (false == button->is_pressed) {
			/* to był dwuklik */
			if (button->callback) {
				(button->callback)(BUTTON_DOUBLE_CLICK_EVENT);
				(button->callback)(BUTTON_PRESS_UP_EVENT);
			}
			
			button->state = 0U;
		}
		
		break;
		
	case 4U:
		/* oczekiwanie na zwolnienie przycisku po długim wciśnięciu */
		if (false == button->is_pressed) {

			if (NULL != button->callback) {
				(button->callback)(
						BUTTON_LONG_PRESS_STOP_EVENT);
				(button->callback)(BUTTON_PRESS_UP_EVENT);
			}
			
			button->state = 0U;
			button->long_repeat_tick_cnt = 0U;
		} else {
			/* śledzenie długiego wciśnięcia,
			 uaktualnij licznik powtórzeń */
			if (0U < (button->long_repeat_tick_cnt)) {
				(button->long_repeat_tick_cnt)--;
			} else {
				/* wykonanie akcji cyklicznej przy długim
				 wciśnięciu */
				if (NULL != button->callback) {
					(button->callback)(
							BUTTON_LONG_PRESS_HOLD_EVENT);
				}
				
				button->long_repeat_tick_cnt =
						button->long_repeat_ticks_nr;
			}
		}
		
		break;
		
	default:
		;
		break;
	}
}

